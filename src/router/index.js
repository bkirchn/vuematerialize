import Vue from 'vue'
import Router from 'vue-router'

import Home from '@/views/Home'
import Search from '@/views/search'

import Admin from '@/views/Admin/Admin'
import AdminDashboard from '@/views/Admin/Dashboard'

import Authoring from '@/views/Authoring/Authoring'
import AuthoringDashboard from '@/views/Authoring/Dashboard'

Vue.use(Router)

export default new Router({
  mode: 'history',
  linkActiveClass: 'active',
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    }, {
      path: '/search',
      name: 'search',
      component: Search
    }, {
      path: '/admin',
      name: 'admin',
      redirect: '/admin/dashboard',
      component: Admin,
      children: [
        {
          path: 'dashboard',
          name: 'admindashboard',
          component: AdminDashboard
        }
      ]
    }, {
      path: '/authoring',
      name: 'authoring',
      redirect: '/authoring/dashboard',
      component: Authoring,
      children: [
        {
          path: 'dashboard',
          name: 'authoringdashboard',
          component: AuthoringDashboard
        }
      ]
    }
  ]
})
